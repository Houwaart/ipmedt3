window.onload = () =>{
  const cardboard = document.getElementById("js--cardboard");
  const placeholders = document.getElementsByClassName("js--placeholder");
  const scene = document.getElementById("js--scene");

  const focusButton = document.getElementsByClassName("js--focusbutton");

  const instructionWindow = document.getElementById("js--instructionWindow");
  const windowImage = document.getElementById("js--windowImage");
  const instructionTitle = document.getElementsByClassName("js--instructionTitle");
  const nextButton = document.getElementsByClassName("js--nextButton");
  const windowContent = document.getElementsByClassName("js--windowContent");

  const focusButtonOFF = document.querySelector('.js--focusbutton');
  const instructionWindowOFF = document.querySelector('#js--instructionWindow');
  const windowImageOFF = document.querySelector('#js--windowImage');
  const windowContentOFF = document.querySelector('.js--windowContent');
  const cardboardOFF =document.querySelector('#js--cardboard');
  const nextButtonOFF = document.querySelector('.js--nextButton');


  const itemsWindow = document.getElementById("js--items_window");
  const itemsImage = document.getElementsByClassName("js--items_image");
  const itemsTitle = document.getElementsByClassName("js--items_title");
  const items = document.getElementsByClassName("js--items");


  const televisionInstructions = document.getElementById("js--tvinstructies");

  let holdingState = null;
  let itemHolding = null;
  let stepState = "step3";
  var dropBox = document.getElementById("js--dropBox");  //onzichtbare element
  const itemsClick = document.getElementsByClassName('js--items_image_description');
  const itemsClickDescription = document.getElementsByClassName('js--items_image_description_text');
  const startVRIntroButton = document.getElementsByClassName("js--button");
  const cursor = document.getElementById("cursorid");

  const deuvelsOrderArray = [];
  const deuvelsMiddenOrderArray = [];
  const deuvelsLastOrderArray = [];
  const parent = document.getElementsByClassName("js--parent");

  const cameraSpheres = document.getElementsByClassName("js--cameraPlane");

  const language = "Dutch Male";

  const steps = ["Stap 1, de bodem. Maak je doos open en Pak de bodem plank (onderdeel A1) uit het menu. Zet het onderdeel op de juiste plek op de mat.", "Stap 2: de linker plank. Pak de zijkant plank (onderdeel B1) uit het menu. Zet het onderdeel op de juiste plek op de bodem plank (A1).", "Stap 3: vastschroeven. pak een schroef (onderdeel s) uit het menu. Verplaats de schroef naar een blauwe cirkel. Schroef deze vast met de schroevendraaier. Herhaal dit nog één keer", "Stap 4: de deuvels. Pak een deuvel (onderdeel D) uit het menu. verplaats de deuvel naar een blauwe cirkel. Herhaal dit tot er geen blauwe cirkels meer zijn.", "Stap 5: plankjes. Pak een klein plankje (onderdeel C) uit het menu. Zet het plankje vast op twee deuvels. Herhaal dit nog 2 keer.", "Stap 6: meer deuvels. Pak een deuvel (onderdeel D) uit het menu. verplaats de deuvel naar een blauwe cirkel. Herhaal dit tot er geen blauwe cirkels meer zijn.", "Stap 7: de midden plank. Pak de midden plank (onderdeel F1) uit het menu. Zet de plank in het midden en maak deze vast aan de deuvels.", "Stap 8: plankjes. Pak een klein plankje (onderdeel C) uit het menu. Zet het plankje vast op twee deuvels. Herhaal dit nog 2 keer.", "Stap 9: nog meer deuvels. Pak een deuvel (onderdeel D) uit het menu. verplaats de deuvel naar een blauwe cirkel. Herhaal dit tot er geen blauwe cirkels meer zijn.","Stap 10: de rechter plank. Pak de zijkant plank (onderdeel B1) uit het menu. Zet het onderdeel op de juiste plek op de bodem plank (A1).", "Stap 11: vastschroeven. pak een schroef (onderdeel s) uit het menu. Verplaats de schroef naar een blauwe cirkel. Schroef deze vast met de schroevendraaier. Herhaal dit nog één keer.", "Stap 12: de bovenkant. Pak de bodem plank (onderdeel A1) uit het menu. Zet het onderdeel op de kast.", "Stap 13: vastschroeven bovenkant. pak een schroef (onderdeel s) uit het menu. Verplaats de schroef naar een blauwe cirkel. Schroef deze vast met de schroevendraaier. Herhaal dit nog drie keer.","Je bent klaar, gefeliciteerd met het assembleren van je kallax kast. "];

  readText = (text) => {
    responsiveVoice.speak(text, language, {rate: 0.9});
  }

  const read = (stepNumber) => {
    readText(steps[stepNumber]);
  }

  const language = "Dutch Male";

  const steps = ["Stap 1, de bodem. Pak de bodem plank (onderdeel A1) uit het menu. Zet het onderdeel op de juiste plek op de mat.", "Stap 2: de linker plank. Pak de zijkant plank (onderdeel B1) uit het menu. Zet het onderdeel op de juiste plek op de bodem plank (A1).", "Stap 3: vastschroeven. pak een schroef (onderdeel s) uit het menu. Verplaats de schroef naar een blauwe cirkel. Schroef deze vast met de schroevendraaier. Herhaal dit nog één keer", "Stap 4: de deuvels. Pak een deuvel (onderdeel D) uit het menu. verplaats de deuvel naar een blauwe cirkel. Herhaal dit tot er geen blauwe cirkels meer zijn.", "Stap 5: plankjes. Pak een klein plankje (onderdeel C) uit het menu. Zet het plankje vast op twee deuvels. Herhaal dit nog 2 keer.", "Stap 6: meer deuvels. Pak een deuvel (onderdeel D) uit het menu. verplaats de deuvel naar een blauwe cirkel. Herhaal dit tot er geen blauwe cirkels meer zijn.", "Stap 7: de midden plank. Pak de midden plank (onderdeel F1) uit het menu. Zet de plank in het midden en maak deze vast aan de deuvels.", "Stap 8: plankjes. Pak een klein plankje (onderdeel C) uit het menu. Zet het plankje vast op twee deuvels. Herhaal dit nog 2 keer.", "Stap 9: nog meer deuvels. Pak een deuvel (onderdeel D) uit het menu. verplaats de deuvel naar een blauwe cirkel. Herhaal dit tot er geen blauwe cirkels meer zijn.","Stap 10: de rechter plank. Pak de zijkant plank (onderdeel B1) uit het menu. Zet het onderdeel op de juiste plek op de bodem plank (A1).", "Stap 11: vastschroeven. pak een schroef (onderdeel s) uit het menu. Verplaats de schroef naar een blauwe cirkel. Schroef deze vast met de schroevendraaier. Herhaal dit nog één keer.", "Stap 12: de bovenkant. Pak de bodem plank (onderdeel A1) uit het menu. Zet het onderdeel op de kast.", "Stap 13: vastschroeven bovenkant. pak een schroef (onderdeel s) uit het menu. Verplaats de schroef naar een blauwe cirkel. Schroef deze vast met de schroevendraaier. Herhaal dit nog drie keer."];

  readText = (text) => {
    responsiveVoice.speak(text, language, {rate: 0.9});
  }

  const read = (stepNumber) => {
    readText(steps[stepNumber]);
  }

  AFRAME.registerComponent('model-opacity', {
    schema: {default: 1.0},
    init: function () {
      this.el.addEventListener('model-loaded', this.update.bind(this));
    },
    update: function () {
      var mesh = this.el.getObject3D('mesh');
      var data = this.data;
      if (!mesh) { return; }
      mesh.traverse(function (node) {
        if (node.isMesh) {
          node.material.opacity = data;
          node.material.transparent = data < 1.0;
          node.material.needsUpdate = true;
        }
      });
    }
  });

  cursor.setAttribute("position","0 0 -4");
  for(let i = 0; i<startVRIntroButton.length;i++){
    startVRIntroButton[i].setAttribute("animation","property: opacity; from: 0; to: 1");
  }

  startVRIntroButton[0].onclick= (event)=>{
      let clickButtonOFF = document.querySelector('.js--button');
      startVRIntroButton[0].onclick = null;
      focusButton[0].setAttribute("class","js--focusbutton clickonhover");
      for(let o = 0; o<startVRIntroButton.length;o++){
        startVRIntroButton[o].setAttribute("animation","property: opacity; from: 1; to: 0");
        if(o == 0){
          setTimeout(function(){clickButtonOFF.parentNode.removeChild(clickButtonOFF);}, 500);
        }
        focusButton[o].setAttribute("animation","property: opacity; from: 0; to: 1");
      }
      focusButton[0].onclick= (event)=>{
        focusButton[0].onclick = null;
        hideIntroductionVR();
        setTimeout(function(){showWindowContent();}, 2000);
      }
  }


  function hideIntroductionVR(){
    cardboard.setAttribute("animation","property: opacity; from: 1; to: 0");
    setTimeout(function(){cardboardOFF.parentNode.removeChild(cardboardOFF)}, 1000);
    setTimeout(function(){focusButtonOFF.parentNode.removeChild(focusButtonOFF)}, 1000);
    for(let i = 0; i<focusButton.length;i++){
      focusButton[i].setAttribute("animation","property: opacity; from: 1; to: 0");
    }
  }

  function showWindowContent(){
    instructionWindow.setAttribute("animation","property: opacity; from: 0; to: 1");
    windowImage.setAttribute("animation","property: opacity; from: 0; to: 1");
    for(let i = 0;i<instructionTitle.length;i++){
      instructionTitle[i].setAttribute("animation","property: opacity; from: 0; to: 1");
      nextButton[i].setAttribute("animation","property: opacity; from: 0; to: 1");
    }
    for(let a = 0; a<windowContent.length;a++){
      windowContent[a].setAttribute("animation","property: opacity; from: 0; to: 1");
    }
    nextButton[0].setAttribute("class","js--nextButton clickonhover")
    nextButton[0].onclick = (event) =>{
      nextButton[0].onclick = null;
      startApplicatie();
    }
  }

  function startApplicatie(){
    clearIntroduction();
    cursor.setAttribute("position","0 0 0");
    read(0)
    setTimeout(function(){
      televisionInstructions.setAttribute("animation","property: opacity; from: 0; to: 1");
    }),1000;

    setTimeout(function(){
<<<<<<< HEAD
      dropBox.innerHTML += '<a-box dynamic-body model-opacity="0" class="js--dropIkeaBox clickonhover" gltf-model="#ikea_doos" scale = "0.8 0.8 0.8" position="0.970 0.063 -3.347" rotation="0 158.728 0"></a-box>';
=======
      dropBox.innerHTML += '<a-box model-opacity="0" class="js--dropIkeaBox clickonhover" gltf-model="#ikea_doos" scale = "0.8 0.8 0.8" position="0.970 0.117 -3.347" rotation="0 158.728 0"></a-box>';
>>>>>>> Mauriccio
      let ikeaBox = document.getElementsByClassName("js--dropIkeaBox");
      let ikeaBoxOFF = document.querySelector(".js--dropIkeaBox");
      ikeaBox[0].setAttribute("animation","property: model-opacity; to: 1");
      ikeaBox[0].onclick = (event) =>{
        ikeaBox[0].onclick = null;
        ikeaBox[0].setAttribute("animation","property: model-opacity; from: 1; to: 0; dur: 1000");
        setTimeout(function(){
          ikeaBoxOFF.parentNode.removeChild(ikeaBoxOFF);
          for(let i = 0; i<cameraSpheres.length; i++){
            cameraSpheres[i].setAttribute("animation","property: opacity; from: 0; to: 1");
          }
          cameraSpheres[0].setAttribute("checkpoint","offset: 0 0 0;")
          cameraSpheres[1].setAttribute("checkpoint","offset: 0 1 0;")
          cameraSpheres[2].setAttribute("checkpoint","offset: 0 1 0;");
          cameraSpheres[3].setAttribute("checkpoint","offset: 0 1 0;");
        },1200);
        items[0].setAttribute("animation","property: opacity; from: 0; to: 1; dur: 1000");
<<<<<<< HEAD
        addPlaceholderListener();
        setTimeout(function(){viewBoxitems("on");
      read(0);},1000);
=======

        setTimeout(function(){viewBoxitems("on");
      ;},1000);
>>>>>>> Mauriccio
      }},2000);
    }

  function clearIntroduction(){
      instructionWindow.setAttribute("animation","property: opacity; from: 1; to: 0");
      windowImage.setAttribute("animation","property: opacity; from: 1; to: 0");
      for(let i = 0;i<instructionTitle.length;i++){
        instructionTitle[i].setAttribute("animation","property: opacity; from: 1; to: 0");
        nextButton[i].setAttribute("animation","property: opacity; from: 1; to: 0");
        windowContent[i].setAttribute("animation","property: opacity; from: 1; to: 0");
      }
    setTimeout(function(){instructionWindowOFF.parentNode.removeChild(instructionWindowOFF)}, 1000);
    setTimeout(function(){nextButtonOFF.parentNode.removeChild(nextButtonOFF)}, 1000);
  }



  function viewBoxitems(state){
    let opacity1 = null;
    let opacity2 = null;
    addPlaceholderListener();
    if(state=="on"){
      opacity1 = "0";
      opacity2 = "1";
      setEventTriggers("PICKITEM");
    }
    for(let i=0;i<itemsClick.length;i++){
      itemsClick[i].onmouseenter = (event) =>{
        itemsClick[i].setAttribute("color","#FF7F50");
      }
      itemsClick[i].onmouseleave = (event) =>{
        itemsClick[i].setAttribute("color","grey");
      }
    }
    itemsWindow.setAttribute("animation","property: opacity; from:"+ opacity1 + "; to:"+ opacity2 +"; dur: 700");
    for(let i =0;i<itemsImage.length;i++){
      itemsImage[i].setAttribute("animation","property: opacity; from:"+ opacity1 + "; to:"+ opacity2 +"; dur: 700");
    }
    for(let i =0;i<itemsClick.length;i++){
      itemsClick[i].setAttribute("animation","property: opacity; from:"+ opacity1 + "; to:"+ opacity2 +"; dur: 700");
    }

    for(let i = 0;i<itemsClickDescription.length;i++){
      itemsClickDescription[i].setAttribute("animation","property: opacity; from:"+ opacity1 + "; to:"+ opacity2 +"; dur: 700");
    }
    for(let i = 0;i<itemsTitle.length; i++){
      itemsTitle[i].setAttribute("animation","property: opacity; from:"+ opacity1 + "; to:"+ opacity2 +"; dur: 700");
    }
  }



  function setEventTriggers(condition){
    if(condition == "PICKITEM"){
      for(let i = 0;i<itemsClick.length;i++){
        itemsClick[i].onclick = (event) =>{
          if(holdingState == "holding"){
            holdingState= "standby";
            document.getElementById("js--hold").remove();
          }
          if(i == 0){ //dikke plank (a1)
            camera.innerHTML += '<a-entity id="js--hold" gltf-model="#dikke_plank" position="0.04 -0.02 -0.04" scale = "0.01 0.01 0.01" rotation="160 90 90"></a-entity>';
            //addPlaceholderListener();
            holdingState= "holding";
            console.log(holdingState);
            itemHolding = "dikkeplank";
          }
          if(i == 1){ //buiten plank (b2)
            console.log("buitenplank");
            camera.innerHTML += '<a-entity id="js--hold" gltf-model="#buiten_plank" position="0.04 -0.02 -0.04" scale = "0.01 0.005 0.01" rotation="160 90 0"></a-entity>';
            holdingState= "holding";
            itemHolding = "buitenplank";
          }
          if(i == 2){
            console.log("kleineplank");
            camera.innerHTML += '<a-entity id="js--hold" gltf-model="#kleine_plank" position="0.04 -0.02 -0.04" scale = "0.015 0.012 0.015" rotation="160 90 90"></a-entity>';
            holdingState= "holding";
            itemHolding = "kleineplank";
          }
          if(i == 3){
            console.log("middenplank");
            camera.innerHTML += '<a-entity id="js--hold" gltf-model="#midden_plank" position="0.04 -0.02 -0.04" scale = "0.01 0.005 0.01" rotation="160 90 0"></a-entity>';
            holdingState= "holding";
            itemHolding = "middenplank";
          }
          if(i == 4){ //schroef (s)
            console.log("schroef");
            camera.innerHTML += '<a-entity id="js--hold" gltf-model="#schroef" position="0.04 -0.02 -0.04" scale = "0.1 0.1 0.1" rotation="160 90 180"></a-entity>';
            holdingState= "holding";
            itemHolding = "schroef";
          }
          if(i == 5){ //deuvel (d)
            console.log("deuvel");
            camera.innerHTML += '<a-entity id="js--hold" gltf-model="#deuvel" position="0.04 -0.02 -0.04" scale = "0.1 0.1 0.1" rotation="160 90 120"></a-entity>';
            holdingState= "holding";
            itemHolding = "deuvel";
          }

        }
      }
    }
  }


<<<<<<< HEAD
  function step1(){
      if(itemHolding == "dikkeplank"){
        document.getElementById("js--hold").remove();
        holdingState= "standby";
        let dikkeplank_a1 = document.createElement("a-entity");
        dikkeplank_a1.setAttribute("class", "js--pickup");
        dikkeplank_a1.setAttribute("gltf-model", "#dikke_plank");
        dikkeplank_a1.setAttribute("position", "0 -0.045 -2.1");
        dikkeplank_a1.setAttribute("scale", "0.48 0.45 0.6");
        dikkeplank_a1.setAttribute("rotation","90 0 0");
        parent[0].appendChild(dikkeplank_a1);
        placeholders[0].setAttribute("class","js--placeholder");
        step2();
      }
  }

  function simulate(){
    cursor.setAttribute("position","0 0 0");
    let dikkeplank_a1 = document.createElement("a-entity");
    dikkeplank_a1.setAttribute("class", "js--pickup");
    dikkeplank_a1.setAttribute("gltf-model", "#dikke_plank");
    dikkeplank_a1.setAttribute("position", "0 -0.045 -2.1");
    dikkeplank_a1.setAttribute("scale", "0.48 0.45 0.6");
    dikkeplank_a1.setAttribute("rotation","90 0 0");
    parent[0].appendChild(dikkeplank_a1); //ONDERKANT

    let dikkeplank_a2 = document.createElement("a-entity");
    dikkeplank_a2.setAttribute("class", "js--dikkeplank");
    dikkeplank_a2.setAttribute("gltf-model", "#dikke_plank");
    dikkeplank_a2.setAttribute("position", "-0.002 -0.045 -5.107");
    dikkeplank_a2.setAttribute("scale", "0.48 0.3 0.6");
    dikkeplank_a2.setAttribute("rotation","-90 0 0");
    //parent[0].appendChild(dikkeplank_a2); //BOVENKANT

    let buitenplank_b1 = document.createElement("a-entity");
    buitenplank_b1.setAttribute("class", "js--pickup");
    buitenplank_b1.setAttribute("gltf-model", "#buiten_plank");
    buitenplank_b1.setAttribute("position", "-1.130 -0.045 -3.618");
    buitenplank_b1.setAttribute("scale", "0.6 0.35 0.6");
    buitenplank_b1.setAttribute("rotation","90 0 180");
    parent[0].appendChild(buitenplank_b1); //RECHTERPLANK

    let buitenplank_b2 = document.createElement("a-entity");
    buitenplank_b2.setAttribute("class", "js--pickup");
    buitenplank_b2.setAttribute("gltf-model", "#buiten_plank");
    buitenplank_b2.setAttribute("position", "1.122 -0.045 -3.618");
    buitenplank_b2.setAttribute("scale", "0.6 0.35 0.6");
    buitenplank_b2.setAttribute("rotation","90 0 0");
    //parent[0].appendChild(buitenplank_b2); //LINKERPLANK


    let middenplank_f1 = document.createElement("a-entity");
    middenplank_f1.setAttribute("class", "js--pickup");
    middenplank_f1.setAttribute("gltf-model", "#midden_plank");
    middenplank_f1.setAttribute("position", "0 -0.045 -3.618");
    middenplank_f1.setAttribute("scale", "0.6 0.35 0.6");
    middenplank_f1.setAttribute("rotation","90 0 0");
    //parent[0].appendChild(middenplank_f1);

    let kleine_plank_c = document.createElement("a-entity");
    kleine_plank_c.setAttribute("class", "js--pickup");
    kleine_plank_c.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_c.setAttribute("position", "-0.55 -0.045 -4.35");
    kleine_plank_c.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_c.setAttribute("rotation","90 90 90");
    //parent[0].appendChild(kleine_plank_c);

    let kleine_plank_d = document.createElement("a-entity");
    kleine_plank_d.setAttribute("class", "js--kleine_plank_midden");
    kleine_plank_d.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_d.setAttribute("position", "0.55 -0.045 -4.35");
    kleine_plank_d.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_d.setAttribute("rotation","90 90 90");
    parent[0].appendChild(kleine_plank_d);

    let kleine_plank_e = document.createElement("a-entity");
    kleine_plank_e.setAttribute("class", "js--kleine_plank_midden");
    kleine_plank_e.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_e.setAttribute("position", "0.55 -0.045 -3.62");
    kleine_plank_e.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_e.setAttribute("rotation","90 90 90");
    parent[0].appendChild(kleine_plank_e);

    let kleine_plank_f = document.createElement("a-entity");
    kleine_plank_f.setAttribute("class", "js--pickup");
    kleine_plank_f.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_f.setAttribute("position", "-0.55 -0.045 -3.62");
    kleine_plank_f.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_f.setAttribute("rotation","90 90 90");
    //parent[0].appendChild(kleine_plank_f);

    let kleine_plank_g = document.createElement("a-entity");
    kleine_plank_g.setAttribute("class", "js--kleine_plank_midden");
    kleine_plank_g.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_g.setAttribute("position", "0.55 -0.045 -2.92");
    kleine_plank_g.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_g.setAttribute("rotation","90 90 90");
    parent[0].appendChild(kleine_plank_g);

    let kleine_plank_h = document.createElement("a-entity");
    kleine_plank_h.setAttribute("class", "js--pickup");
    kleine_plank_h.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_h.setAttribute("position", "-0.55 -0.045 -2.92");
    kleine_plank_h.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_h.setAttribute("rotation","90 90 90");
    //parent[0].appendChild(kleine_plank_h);
    parent[0].appendChild(dikkeplank_a1);



    let deuvel = document.createElement("a-entity");
    deuvel.setAttribute("class", "js--deuvel_kleineplank");
    deuvel.setAttribute("gltf-model", "#deuvel");
    deuvel.setAttribute("position", "0 0.33 -4.359");
    deuvel.setAttribute("scale", "0.9 0.830 0.650");
    //parent[0].appendChild(deuvel);
    cursorid.setAttribute("position","0 0 0");


    /*let cameraSphereMiddle = document.createElement("a-sphere");
    cameraSphereMiddle.setAttribute("class", "js--cameraspheremiddle clickonhover");
    cameraSphereMiddle.setAttribute("color", "blue");
    cameraSphereMiddle.setAttribute("position", "0 0.75 -2");
    cameraSphereMiddle.setAttribute("opacity", "0");
    cameraSphereMiddle.setAttribute("radius","0.3");
    cameraSphereMiddle.setAttribute("checkpoint","offset: 0 0 0");
    scene.appendChild(cameraSphereMiddle);
    let camerasphereMiddle = document.getElementsByClassName("js--cameraspheremiddle");
    camerasphereMiddle[0].setAttribute("opacity","1");*/
    parent[0].setAttribute("rotation","90 -90 0");
    parent[0].setAttribute("position","-0.120 -2.64 -3.700");

    step7();
    viewBoxitems("on");
  }
  function step7_5(){
    read(8);
    for(let i = 0; i<6;i++){
      let indicatorDeuvel= document.createElement("a-plane");
      indicatorDeuvel.setAttribute("class","js--indicatorDeuvel2 clickonhover");


      indicatorDeuvel.setAttribute("geometry", "primitive: circle; radius: 0.05;");
      indicatorDeuvel.setAttribute("color","blue");
      indicatorDeuvel.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1300; easing: linear");
      if(i == 0){
        indicatorDeuvel.setAttribute("position", "-0.435 1.71 -2.661");
      }
      if(i == 1){
        indicatorDeuvel.setAttribute("position", "0.34 1.71 -2.661");
      }
      if(i == 2){
        indicatorDeuvel.setAttribute("position", "-0.435 0.983 -2.661");
      }
      if(i == 3){
        indicatorDeuvel.setAttribute("position", "0.34 0.983 -2.661");
      }
      if(i == 4){
        indicatorDeuvel.setAttribute("position", "-0.435 0.27 -2.661");
      }
      if(i == 5){
        indicatorDeuvel.setAttribute("position", "0.34 0.27 -2.661");
      }
      let indicatorDeuvelClass= document.getElementsByClassName("js--indicatorDeuvel2");
      scene.appendChild(indicatorDeuvel);
      indicatorDeuvelClass[i].onclick=(event)=>{
        if(itemHolding == "deuvel"){
          step7_5_generatedeuvels(i);
          indicatorDeuvelClass[i].setAttribute("visible","false");
          indicatorDeuvelClass[i].onclick = null;
          indicatorDeuvelClass[i].setAttribute("class","js--indicatorDeuvel2");
          if(deuvelsLastOrderArray.length == 6 && deuvelsLastOrderArray.includes(undefined) == false){
            televisionInstructions.setAttribute("opacity","1");
            televisionInstructions.setAttribute("src","#stap10");
            document.getElementById("js--hold").remove();
            holdingState = "standby";
            step9();
          }
        }
      }
    }


    //step8();
  }
  function step7_5_generatedeuvels(a){
    let indicatorDeuvelClass = document.getElementsByClassName("js--indicatorDeuvel");
    if(a == 0){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 0.33 -4.359");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }
    if(a == 1){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 -0.45 -4.359");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }
    if(a == 2){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 0.33 -3.619");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }
    if(a == 3){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 -0.45 -3.619");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }
=======



>>>>>>> Mauriccio

    if(a == 4){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 0.33 -2.926");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }
    if(a == 5){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 -0.45 -2.926");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }
    console.log(deuvelsLastOrderArray);
  }
  function addPlaceholderListener(){
    placeholders[0].setAttribute("class","js--placeholder clickonhover");
    placeholders[0].onclick = (event) =>{
      placeholders[0].onclick = null;
      console.log("hi");
      step1();
    }
  }

  function eventFire(el, etype){
    if(el.fireEvent) {
      el.fireEvent('on' + etype);
    }
    else{
      let evObj = document.createEvent('Events');
      evObj.initEvent(etype, true, false);
      el.dispatchEvent(evObj);
    }
  }
  function step1(){
      if(itemHolding == "dikkeplank"){
        document.getElementById("js--hold").remove();
        holdingState= "standby";
        let dikkeplank_a1 = document.createElement("a-entity");
        dikkeplank_a1.setAttribute("class", "js--pickup");
        dikkeplank_a1.setAttribute("gltf-model", "#dikke_plank");
        dikkeplank_a1.setAttribute("position", "0 -0.045 -2.1");
        dikkeplank_a1.setAttribute("scale", "0.48 0.45 0.6");
        dikkeplank_a1.setAttribute("rotation","90 0 0");
        parent[0].appendChild(dikkeplank_a1);
        placeholders[0].setAttribute("class","js--placeholder");
        step2();
      }
  }

  function step2(){
    televisionInstructions.setAttribute("src","#stap2");
    read(1);
    let indicatorB1 = document.createElement("a-plane");
    indicatorB1.setAttribute("class", "js--indicatorStep2 clickonhover");
    indicatorB1.setAttribute("color","blue");
    indicatorB1.setAttribute("position", "-1.041 -0.675 -3.578");
    indicatorB1.setAttribute("width","3.2");
    indicatorB1.setAttribute("height","0.35");
    indicatorB1.setAttribute("rotation","-90 90 0");
    indicatorB1.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1000; loop: true");
    indicatorB1.setAttribute("opacity","1");
    parent[0].appendChild(indicatorB1);
    let indicatorB1OFF = document.querySelector(".js--indicatorStep2");
    indicatorB1.onclick = (event) =>{
      if(itemHolding == "buitenplank"){
        indicatorB1.onclick = null;
        document.getElementById("js--hold").remove();
        holdingState="standby";
        indicatorB1OFF.parentNode.removeChild(indicatorB1OFF);
        let buitenplank_b1 = document.createElement("a-entity");
        buitenplank_b1.setAttribute("class", "js--pickup");
        buitenplank_b1.setAttribute("gltf-model", "#buiten_plank");
        buitenplank_b1.setAttribute("position", "-1.12 -0.045 -3.628");
        buitenplank_b1.setAttribute("scale", "0.6 0.35 0.6");
        buitenplank_b1.setAttribute("rotation","90 0 180");
        parent[0].appendChild(buitenplank_b1);
        step3();
      }
    }
  }

  function step3(){
    read(2);
    televisionInstructions.setAttribute("src","#stap3");
    let indicatorSchroef = document.createElement("a-plane");
    indicatorSchroef.setAttribute("class","js--indicatorSchroef clickonhover");
    indicatorSchroef.setAttribute("position", "-1.138 0.445 -2.014");
    indicatorSchroef.setAttribute("geometry", "primitive: circle; radius: 0.045;");
    indicatorSchroef.setAttribute("color","blue");
    indicatorSchroef.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1300; loop: true;");
    parent[0].appendChild(indicatorSchroef);
    let cameraIndicator = document.createElement("a-plane");
    cameraIndicator.setAttribute("class","js--cameraPlaneMiddle clickonhover")
    cameraIndicator.setAttribute("position", "0 -0.2 -0.7");
    cameraIndicator.setAttribute("scale", "1.5 1 1");
    cameraIndicator.setAttribute("rotation", "-80 0 0");
    cameraIndicator.setAttribute("material","side:double; transparent:true;");
    cameraIndicator.setAttribute("src","#cameraplane");
    cameraIndicator.setAttribute("checkpoint","offset: 0 0.5 0;");
    parent[0].appendChild(cameraIndicator);

    cameraIndicator.onclick = (event) =>{
      cameraIndicator.setAttribute("opacity","0.2");
      cameraIndicator.setAttribute("rotation","-90 0 0");
      cameraIndicator.setAttribute("position","0 -0.6 -0.7");
    }
    cameraSpheres[0].onclick = (event) =>{
      cameraIndicator.setAttribute("opacity","1");
      cameraIndicator.setAttribute("rotation","-80 0 0");
      cameraIndicator.setAttribute("position","0 -0.2 -0.7");
    }
      indicatorSchroef.onclick = (event) =>{
        if(itemHolding == "schroef"){
          indicatorSchroef.onclick = null;
          document.getElementById("js--hold").remove();
          holdingState="standby";
          itemHolding=null;
          indicatorSchroef.setAttribute("visible","false");
          let schroefBovenkant = document.createElement("a-entity");
          schroefBovenkant.setAttribute("class", "js--schroefBovenkant");
          schroefBovenkant.setAttribute("gltf-model", "#schroef");
          schroefBovenkant.setAttribute("position", "-1.138 0.445 -1.903");
          schroefBovenkant.setAttribute("scale", "0.6 0.6 0.6");
          schroefBovenkant.setAttribute("rotation","90 0 0");
          parent[0].appendChild(schroefBovenkant);

          let schroevendraaierItem = document.createElement("a-entity");
          schroevendraaierItem.setAttribute("class", "js--schroevendraaierIdle clickonhover");
          schroevendraaierItem.setAttribute("gltf-model", "#schroevendraaier");
          schroevendraaierItem.setAttribute("position", "-1.132 -0.604 -1.575");
          schroevendraaierItem.setAttribute("scale", "0.6 0.6 0.6");
          schroevendraaierItem.setAttribute("rotation","90 -40.000 0");
          parent[0].appendChild(schroevendraaierItem);

          schroevendraaierItem.onclick = (event) =>{
            schroevendraaierItem.onclick = null;
            schroevendraaierItem.setAttribute("model-opacity","0");
            step3_schroevendraaier();
          }
        }
      }
    }

  function step3_schroevendraaier(item){
    if(stepState == "step3"){
      let schroevendraaierItem2 = document.createElement("a-entity");
      schroevendraaierItem2.setAttribute("class", "js--schroevendraaier clickonhover");
      schroevendraaierItem2.setAttribute("gltf-model", "#schroevendraaier");
      schroevendraaierItem2.setAttribute("position", "-1.132 0.45 -1.45");
      schroevendraaierItem2.setAttribute("scale", "0.6 0.6 0.6");
      schroevendraaierItem2.setAttribute("rotation","90 0 0");
      parent[0].appendChild(schroevendraaierItem2);

    let schroefBovenkantClass = document.getElementsByClassName("js--schroefBovenkant");
    schroevendraaierItem2.onclick = (event) =>{
      schroefBovenkantClass[0].setAttribute("position","-1.138 0.445 -2.117");
      schroevendraaierItem2.onclick = null;
      schroevendraaierItem2.setAttribute("position", "-1.132 0.45 -1.8");
      setTimeout(function(){
        schroevendraaierItem2.setAttribute("model-opacity","0");
      },2000);
      step3_2(schroevendraaierItem2);
      stepState = "step3_final";
      }
    }

    if(stepState == "step3_final"){
      item.onclick = (event) =>{
        item.onclick = null;
        item.setAttribute("position", "-1.132 -0.50 -1.8");
        let schroefOnderkantClass = document.getElementsByClassName("js--schroefOnderkant");
        schroefOnderkantClass[0].setAttribute("position","-1.132 -0.492 -2.117");
        setTimeout(function(){
          item.setAttribute("model-opacity","0");
          step4();
<<<<<<< HEAD
          read(3);
=======

>>>>>>> Mauriccio
        },2000);
      }
    }
  }

  function step3_2(item,item2){
    let indicatorSchroefClass = document.getElementsByClassName("js--indicatorSchroef");
    let schroevendraaierIndicatorClass = document.getElementsByClassName("js--indicator");
    let schroevendraaierIdle = document.getElementsByClassName("js--schroevendraaierIdle");
    indicatorSchroefClass[0].setAttribute("visible", "true");
    indicatorSchroefClass[0].setAttribute("position", "-1.138 -0.492 -2.014");
    indicatorSchroefClass[0].onclick =(event)=>{
      if(itemHolding =="schroef"){
        indicatorSchroefClass[0].setAttribute("visible", "false");
        document.getElementById("js--hold").remove();
        holdingState="standby";
        setTimeout(function(){
          schroevendraaierIdle[0].setAttribute("model-opacity","1");
          let schroefOnderkant = document.createElement("a-entity");
          schroefOnderkant.setAttribute("class", "js--schroefOnderkant");
          schroefOnderkant.setAttribute("gltf-model", "#schroef");
          schroefOnderkant.setAttribute("position", "-1.132 -0.492 -1.903");
          schroefOnderkant.setAttribute("scale", "0.6 0.6 0.6");
          schroefOnderkant.setAttribute("rotation","90 0 0");
          parent[0].appendChild(schroefOnderkant);

          schroevendraaierIdle[0].onclick=(event)=>{
            schroevendraaierIdle[0].onclick=null;
            schroevendraaierIdle[0].setAttribute("class","js--schroevendraaierIdle");
            schroevendraaierIdle[0].setAttribute("model-opacity","0");
            item.setAttribute("position","-1.132 -0.50 -1.45");
            item.setAttribute("model-opacity","1");
            step3_schroevendraaier(item);
          }
        },100);
      }
    }
  }

  function step4(){
    televisionInstructions.setAttribute("src","#stap4");
    read(3);
    parent[0].setAttribute("rotation","90 -90 0");
    parent[0].setAttribute("position","-0.120 -2.64 -3.700");
    stepState = "step4_begin";
    let cameraSphereMiddle = document.createElement("a-sphere");
    cameraSphereMiddle.setAttribute("class", "js--cameraspheremiddle clickonhover");
    cameraSphereMiddle.setAttribute("color", "gray");
    cameraSphereMiddle.setAttribute("position", "0 0.1 -2");
    cameraSphereMiddle.setAttribute("opacity", "0");
    cameraSphereMiddle.setAttribute("radius","0.3");
    scene.appendChild(cameraSphereMiddle);
    let camerasphereMiddle = document.getElementsByClassName("js--cameraspheremiddle");
    camerasphereMiddle[0].setAttribute("opacity","1");
    camerasphereMiddle[0].onclick=(event)=>{
      if(itemHolding == "deuvel" && stepState == "step4_begin"){
        cameraSphereMiddle.setAttribute("color", "blue");
        camerasphereMiddle[0].onmouseenter=null;
        camerasphereMiddle[0].onmouseleave=null;
        stepState = "step4";
        camerasphereMiddle[0].setAttribute("checkpoint","offset: 0 0 0");
        setTimeout(function(){
          eventFire(camerasphereMiddle[0], 'click');
          setTimeout(function(){step4_generateBlueIndicators()},1000);
        },500);
      }
    }
    camerasphereMiddle[0].onmouseenter=(event)=>{
      if(itemHolding=="deuvel"){
        camerasphereMiddle[0].setAttribute("color","blue");
      }
    }
    camerasphereMiddle[0].onmouseleave=(event)=>{
      camerasphereMiddle[0].setAttribute("color","gray");
    }
  }

<<<<<<< HEAD

  //simulate();
  const deuvelsOrderArray = [];
  const deuvelsMiddenOrderArray = [];
  const deuvelsLastOrderArray = [];
=======
>>>>>>> Mauriccio
  function step4_generateBlueIndicators(){
    for(let i = 0; i<6;i++){
      var indicatorDeuvel= document.createElement("a-plane");
      indicatorDeuvel.setAttribute("class","js--indicatorDeuvel clickonhover");
      var indicatorDeuvelClass= document.getElementsByClassName("js--indicatorDeuvel");
      var indicatorDeuvelfOFF = document.querySelector(".js--indicatorDeuvel");
      indicatorDeuvel.setAttribute("geometry", "primitive: circle; radius: 0.05;");
      indicatorDeuvel.setAttribute("color","blue");
      indicatorDeuvel.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1300; easing: linear");
      if(i == 0){
        indicatorDeuvel.setAttribute("position", "-0.435 1.71 -4.73");
      }
      if(i == 1){
        indicatorDeuvel.setAttribute("position", "0.34 1.71 -4.73");
      }
      if(i == 2){
        indicatorDeuvel.setAttribute("position", "-0.435 0.99 -4.73");
      }
      if(i == 3){
        indicatorDeuvel.setAttribute("position", "0.34 0.99 -4.73");
      }
      if(i == 4){
        indicatorDeuvel.setAttribute("position", "-0.435 0.27 -4.73");
      }
      if(i == 5){
        indicatorDeuvel.setAttribute("position", "0.34 0.27 -4.73");
      }
      scene.appendChild(indicatorDeuvel);
    }
    for(let a=0;a<indicatorDeuvelClass.length;a++){
      indicatorDeuvelClass[a].onclick=(event)=>{
        if(itemHolding == "deuvel"){
        indicatorDeuvelClass[a].onclick = null;
        let deuvelClass = document.getElementsByClassName("js--deuvel");
        indicatorDeuvelClass[a].setAttribute("animation","property: opacity; from:1; to: 0; dur: 1300; easing: linear");
        if(a == 0){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class","js--deuvel clickonhover");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "-1 0.33 -4.352");
          deuvel.setAttribute("scale", "0.300 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsOrderArray[0]=deuvel;
        }
        if(a == 1){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class", "js--deuvel clickonhover");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "-1 -0.45 -4.352");
          deuvel.setAttribute("scale", "0.300 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsOrderArray[1]=deuvel;
        }
        if(a == 2){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class", "js--deuvel clickonhover");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "-1 0.33 -3.635");
          deuvel.setAttribute("scale", "0.300 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsOrderArray[2]=deuvel;
        }
        if(a == 3){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class", "js--deuvel clickonhover");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "-1 -0.45 -3.635");
          deuvel.setAttribute("scale", "0.300 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsOrderArray[3]=deuvel;
        }

        if(a == 4){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class", "js--deuvel clickonhover");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "-1 0.33 -2.91");
          deuvel.setAttribute("scale", "0.300 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsOrderArray[4]=deuvel;
        }
        if(a == 5){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class", "js--deuvel clickonhover");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "-1 -0.45 -2.91");
          deuvel.setAttribute("scale", "0.300 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsOrderArray[5]=deuvel;
        }
        if(deuvelsOrderArray.length == 6 && deuvelsOrderArray.includes(undefined) == false){ //6
          console.log(deuvelsOrderArray);
          for(let i = 0;i<indicatorDeuvelClass.length;i++){
            indicatorDeuvelClass[i].setAttribute("class","js--indicatorDeuvel");
          }
          let sphereCameraMiddleOFF = document.querySelector(".js--cameraspheremiddle");
          sphereCameraMiddleOFF.parentNode.removeChild(sphereCameraMiddleOFF);
          document.getElementById("js--hold").remove();
          holdingState = "standby";
          step5();
        }
      }
    }
    }
  }

  function step5(){
    televisionInstructions.setAttribute("src","#stap5");
    read(4);
    let deuvelClass = document.getElementsByClassName("js--deuvel");
    cameraSpheres[0].onclick = (event) =>{
      cameraSpheres[0].onclick = null;
      let cameraIndicator1 = document.createElement("a-plane");
      cameraIndicator1.setAttribute("class","js--indicatorCamera clickonhover")
      cameraIndicator1.setAttribute("position", "3.400 -0.119 -2.672");
      cameraIndicator1.setAttribute("scale", "1.5 1 1");
      cameraIndicator1.setAttribute("rotation", "0 -20 90.000");
      cameraIndicator1.setAttribute("material","side:double; transparent:true;");
      cameraIndicator1.setAttribute("src","#cameraplane");
      cameraIndicator1.setAttribute("checkpoint","offset: 0 0.9 0;");
      cameraIndicator1.setAttribute("opacity","1");
      parent[0].appendChild(cameraIndicator1);
      cameraIndicator1.onclick = (event) =>{
        cameraIndicator1.setAttribute("opacity","0.2");
        cameraIndicator1.setAttribute("rotation","0 0 90.000");
        cameraIndicator1.setAttribute("position","3.400 -0.119 -1.934");
      }
      cameraSpheres[0].onclick = (event) =>{
        cameraIndicator1.setAttribute("opacity","1");
        cameraIndicator1.setAttribute("rotation","0 -20 90.000");
        cameraIndicator1.setAttribute("position","3.400 -0.119 -2.672");
      }
    }



    for(let i = 0;i<deuvelsOrderArray.length;i++){
      deuvelsOrderArray[i].onclick = (event) =>{
        if(itemHolding == "kleineplank"){
          console.log(i);
          if(i == 0 || i == 1){
            deuvelsOrderArray[0].onclick = null;
            deuvelsOrderArray[1].onclick = null;
            let kleine_plank_c = document.createElement("a-entity");
            kleine_plank_c.setAttribute("class", "js--kleineplank");
            kleine_plank_c.setAttribute("gltf-model", "#kleine_plank");
            kleine_plank_c.setAttribute("position", "-0.55 -0.045 -4.35");
            kleine_plank_c.setAttribute("scale", "0.47 0.6  0.6");
            kleine_plank_c.setAttribute("rotation","90 90 90");
            parent[0].appendChild(kleine_plank_c);
          }
          if(i == 2 || i == 3){
            deuvelsOrderArray[2].onclick = null;
            deuvelsOrderArray[3].onclick = null;
            let kleine_plank_f = document.createElement("a-entity");
            kleine_plank_f.setAttribute("class", "js--kleineplank");
            kleine_plank_f.setAttribute("gltf-model", "#kleine_plank");
            kleine_plank_f.setAttribute("position", "-0.55 -0.045 -3.62");
            kleine_plank_f.setAttribute("scale", "0.47 0.6  0.6");
            kleine_plank_f.setAttribute("rotation","90 90 90");
            parent[0].appendChild(kleine_plank_f);
          }
          if(i == 4 || i == 5){
            deuvelsOrderArray[4].onclick = null;
            deuvelsOrderArray[5].onclick = null;
            let kleine_plank_h = document.createElement("a-entity");
            kleine_plank_h.setAttribute("class", "js--kleineplank");
            kleine_plank_h.setAttribute("gltf-model", "#kleine_plank");
            kleine_plank_h.setAttribute("position", "-0.55 -0.045 -2.92");
            kleine_plank_h.setAttribute("scale", "0.47 0.6  0.6");
            kleine_plank_h.setAttribute("rotation","90 90 90");
            parent[0].appendChild(kleine_plank_h);
          }
          let kleinePlankClass = document.getElementsByClassName("js--kleineplank");
          if(kleinePlankClass.length == 3){
            for(let i=0;i<deuvelsOrderArray.length;i++){
              deuvelsOrderArray[i].setAttribute("class","js--deuvel");
            }
            document.getElementById("js--hold").remove();
            holdingState = "standby";
            televisionInstructions.setAttribute("src","#stap6");
            step6();
          }
        }
      }
    }
    }

  function step6(){
    read(5);
    let indicatorDeuvelClass = document.getElementsByClassName("js--indicatorDeuvel");
    for(let i = 0;i<indicatorDeuvelClass.length;i++){
      indicatorDeuvelClass[i].setAttribute("class","js--indicatorDeuvel clickonhover");
      indicatorDeuvelClass[i].setAttribute("opacity","1");
      indicatorDeuvelClass[i].setAttribute("geometry", "primitive: circle; radius: 0.04;");
      if(i == 0){indicatorDeuvelClass[0].setAttribute("position","-0.437 1.71 -3.75")}
      if(i == 1){indicatorDeuvelClass[1].setAttribute("position","0.33 1.71 -3.75")}
      if(i == 2){indicatorDeuvelClass[2].setAttribute("position","-0.437 0.98 -3.75")}
      if(i == 3){indicatorDeuvelClass[3].setAttribute("position","0.33 0.98 -3.75")}
      if(i == 4){indicatorDeuvelClass[4].setAttribute("position","-0.437 0.28 -3.75")}
      if(i == 5){indicatorDeuvelClass[5].setAttribute("position","0.33 0.28 -3.75")}
      indicatorDeuvelClass[i].onclick=(event)=>{
        if(itemHolding == "deuvel"){
          step6_generateDeuvels(i);
          indicatorDeuvelClass[i].setAttribute("visible","false");
          indicatorDeuvelClass[i].onclick = null;
          indicatorDeuvelClass[i].setAttribute("class","js--indicatorDeuvel");
          if(deuvelsMiddenOrderArray.length == 6 && deuvelsMiddenOrderArray.includes(undefined) == false){
            televisionInstructions.setAttribute("src","#stap7");
            document.getElementById("js--hold").remove();
            holdingState = "standby";
            step7();
          }
        }
      }
    }
  }

  function step6_generateDeuvels(a){
        if(a == 0){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class", "js--deuvel_kleineplank");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "0 0.33 -4.359");
          deuvel.setAttribute("scale", "0.9 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsMiddenOrderArray[a]=deuvel;
        }
        if(a == 1){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class", "js--deuvel_kleineplank");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "0 -0.45 -4.359");
          deuvel.setAttribute("scale", "0.9 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsMiddenOrderArray[a]=deuvel;
        }
        if(a == 2){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class", "js--deuvel_kleineplank");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "0 0.33 -3.619");
          deuvel.setAttribute("scale", "0.9 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsMiddenOrderArray[a]=deuvel;
        }
        if(a == 3){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class", "js--deuvel_kleineplank");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "0 -0.45 -3.619");
          deuvel.setAttribute("scale", "0.9 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsMiddenOrderArray[a]=deuvel;
        }

        if(a == 4){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class", "js--deuvel_kleineplank");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "0 0.33 -2.926");
          deuvel.setAttribute("scale", "0.9 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsMiddenOrderArray[a]=deuvel;
        }
        if(a == 5){
          let deuvel = document.createElement("a-entity");
          deuvel.setAttribute("class", "js--deuvel_kleineplank");
          deuvel.setAttribute("gltf-model", "#deuvel");
          deuvel.setAttribute("position", "0 -0.45 -2.926");
          deuvel.setAttribute("scale", "0.9 0.830 0.650");
          parent[0].appendChild(deuvel);
          deuvelsMiddenOrderArray[a]=deuvel;
        }
        console.log(deuvelsMiddenOrderArray);
  }

  function step7(){
    read(6);
    let indicatorMiddle = document.createElement("a-plane");
    indicatorMiddle.setAttribute("class", "js--indicatorMiddle");
    indicatorMiddle.setAttribute("color","blue");
    indicatorMiddle.setAttribute("position", "-0.1 -0.05 -2.2");
    indicatorMiddle.setAttribute("width","1.21");
    indicatorMiddle.setAttribute("height","0.2");
    indicatorMiddle.setAttribute("rotation","0 180 -90");
    indicatorMiddle.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1000; loop: true");
    parent[0].appendChild(indicatorMiddle);
    indicatorMiddle.onclick =(event)=>{
      if(itemHolding == "middenplank"){
        indicatorMiddle.setAttribute("visible","false");
        indicatorMiddle.removeAttribute("animation");
        indicatorMiddle.onclick = null;
        let middenplank_f1 = document.createElement("a-entity");
        middenplank_f1.setAttribute("class", "js--pickup");
        middenplank_f1.setAttribute("gltf-model", "#midden_plank");
        middenplank_f1.setAttribute("position", "0 -0.045 -3.618");
        middenplank_f1.setAttribute("scale", "0.6 0.35 0.6");
        middenplank_f1.setAttribute("rotation","90 0 0");
        parent[0].appendChild(middenplank_f1);
        televisionInstructions.setAttribute("src","#stap8");
        step8();
      }
    }
  }

<<<<<<< HEAD
  function step8(){
    read(7);
    cameraSpheres[0].onclick=(event)=>{
      cameraSpheres[0].onclick = null;
      let camerasphereMiddle = document.getElementsByClassName("js--cameraspheremiddle");
      camerasphereMiddle[0].setAttribute("position","0 0.1 -1");
=======
  function step7_5(){
    read(8);
    for(let i = 0; i<6;i++){
      let indicatorDeuvel= document.createElement("a-plane");
      indicatorDeuvel.setAttribute("class","js--indicatorDeuvel2 clickonhover");


      indicatorDeuvel.setAttribute("geometry", "primitive: circle; radius: 0.05;");
      indicatorDeuvel.setAttribute("color","blue");
      indicatorDeuvel.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1300; easing: linear");
      if(i == 0){
        indicatorDeuvel.setAttribute("position", "-0.435 1.71 -2.661");
      }
      if(i == 1){
        indicatorDeuvel.setAttribute("position", "0.34 1.71 -2.661");
      }
      if(i == 2){
        indicatorDeuvel.setAttribute("position", "-0.435 0.983 -2.661");
      }
      if(i == 3){
        indicatorDeuvel.setAttribute("position", "0.34 0.983 -2.661");
      }
      if(i == 4){
        indicatorDeuvel.setAttribute("position", "-0.435 0.27 -2.661");
      }
      if(i == 5){
        indicatorDeuvel.setAttribute("position", "0.34 0.27 -2.661");
      }
      let indicatorDeuvelClass= document.getElementsByClassName("js--indicatorDeuvel2");
      scene.appendChild(indicatorDeuvel);
      indicatorDeuvelClass[i].onclick=(event)=>{
        if(itemHolding == "deuvel"){
          step7_5_generatedeuvels(i);
          indicatorDeuvelClass[i].setAttribute("visible","false");
          indicatorDeuvelClass[i].onclick = null;
          indicatorDeuvelClass[i].setAttribute("class","js--indicatorDeuvel2");
          if(deuvelsLastOrderArray.length == 6 && deuvelsLastOrderArray.includes(undefined) == false){
            televisionInstructions.setAttribute("opacity","1");
            televisionInstructions.setAttribute("src","#stap10");
            document.getElementById("js--hold").remove();
            holdingState = "standby";
            step9();
          }
        }
      }
>>>>>>> Mauriccio
    }


    //step8();
  }
  function step7_5_generatedeuvels(a){
    let indicatorDeuvelClass = document.getElementsByClassName("js--indicatorDeuvel");
    if(a == 0){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 0.33 -4.359");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }
    if(a == 1){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 -0.45 -4.359");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }
    if(a == 2){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 0.33 -3.619");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }
    if(a == 3){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 -0.45 -3.619");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }

    if(a == 4){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 0.33 -2.926");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }
    if(a == 5){
      let deuvel = document.createElement("a-entity");
      deuvel.setAttribute("class", "js--deuvel_kleineplank");
      deuvel.setAttribute("gltf-model", "#deuvel");
      deuvel.setAttribute("position", "1.030 -0.45 -2.926");
      deuvel.setAttribute("scale", "0.9 0.830 0.650");
      parent[0].appendChild(deuvel);
      deuvelsLastOrderArray[a]=deuvel;
    }
    console.log(deuvelsLastOrderArray);
  }

  function step8(){
    read(7);

    for(let i = 0; i<deuvelsMiddenOrderArray.length; i++){
      deuvelsMiddenOrderArray[i].setAttribute("class","js--deuvel_kleineplank clickonhover")
      deuvelsMiddenOrderArray[i].onclick=(event)=>{
        if(itemHolding == "kleineplank"){
          if(i == 0 || i == 1){
            deuvelsMiddenOrderArray[0].onclick = null;
            deuvelsMiddenOrderArray[1].onclick = null;
            let kleine_plank_d = document.createElement("a-entity");
            kleine_plank_d.setAttribute("class", "js--kleine_plank_midden");
            kleine_plank_d.setAttribute("gltf-model", "#kleine_plank");
            kleine_plank_d.setAttribute("position", "0.55 -0.045 -4.35");
            kleine_plank_d.setAttribute("scale", "0.47 0.6  0.6");
            kleine_plank_d.setAttribute("rotation","90 90 90");
            parent[0].appendChild(kleine_plank_d);
          }
          if(i == 2 || i == 3){
            deuvelsMiddenOrderArray[2].onclick = null;
            deuvelsMiddenOrderArray[3].onclick = null;
            let kleine_plank_e = document.createElement("a-entity");
            kleine_plank_e.setAttribute("class", "js--kleine_plank_midden");
            kleine_plank_e.setAttribute("gltf-model", "#kleine_plank");
            kleine_plank_e.setAttribute("position", "0.55 -0.045 -3.62");
            kleine_plank_e.setAttribute("scale", "0.47 0.6  0.6");
            kleine_plank_e.setAttribute("rotation","90 90 90");
            parent[0].appendChild(kleine_plank_e);
          }
          if(i == 4 || i ==5){
            deuvelsMiddenOrderArray[4].onclick = null;
            deuvelsMiddenOrderArray[5].onclick = null;
            let kleine_plank_g = document.createElement("a-entity");
            kleine_plank_g.setAttribute("class", "js--kleine_plank_midden");
            kleine_plank_g.setAttribute("gltf-model", "#kleine_plank");
            kleine_plank_g.setAttribute("position", "0.55 -0.045 -2.92");
            kleine_plank_g.setAttribute("scale", "0.47 0.6  0.6");
            kleine_plank_g.setAttribute("rotation","90 90 90");
            parent[0].appendChild(kleine_plank_g);
          }
          let kleinePlankClass=document.getElementsByClassName("js--kleine_plank_midden");
          if(kleinePlankClass.length == 3){
            document.getElementById("js--hold").remove();
            holdingState = "standby";
            televisionInstructions.setAttribute("src","#stap9");
            step7_5();

          }
        }
      }
    }
  }
  function step9(){
    read(9);
    let indicatorMiddle = document.createElement("a-plane");
    indicatorMiddle.setAttribute("class", "js--indicatorMiddle");
    indicatorMiddle.setAttribute("color","blue");
    indicatorMiddle.setAttribute("position", "1.11 -0.045 -2.2");
    indicatorMiddle.setAttribute("width","1.2");
    indicatorMiddle.setAttribute("height","0.2");
    indicatorMiddle.setAttribute("rotation","0 180 -90");
    indicatorMiddle.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1000; loop: true");
    parent[0].appendChild(indicatorMiddle);
    indicatorMiddle.onclick = (event) =>{
      if(itemHolding == "buitenplank"){
        indicatorMiddle.onclick = null;
        indicatorMiddle.setAttribute("visible","false");
        let buitenplank_b2 = document.createElement("a-entity");
        buitenplank_b2.setAttribute("class", "js--pickup");
        buitenplank_b2.setAttribute("gltf-model", "#buiten_plank");
        buitenplank_b2.setAttribute("position", "1.122 -0.045 -3.618");
        buitenplank_b2.setAttribute("scale", "0.6 0.35 0.6");
        buitenplank_b2.setAttribute("rotation","90 0 0");
        parent[0].appendChild(buitenplank_b2); //LINKERPLANK
        document.getElementById("js--hold").remove();
        holdingState = "standby";
        televisionInstructions.setAttribute("src","#stap11");
        step10();
      }
    }
  }
  function step10(){
    read(10);
    parent[0].setAttribute("rotation","0 0 0");
    parent[0].setAttribute("position","0 0 0");
    let indicatorCameraClassOld = document.querySelector(".js--cameraPlaneMiddle");
    indicatorCameraClassOld.parentNode.removeChild(indicatorCameraClassOld);

    let indicatorCameraClass = document.getElementsByClassName("js--indicatorCamera");
    indicatorCameraClass[0].setAttribute("position","0.004 -0.240 -0.622");
    indicatorCameraClass[0].setAttribute("rotation","-70.000 0 0");
    indicatorCameraClass[0].setAttribute("opacity","1");
    let indicatorSchroef = document.createElement("a-plane");
    cameraSpheres[0].onclick=(event)=>{
      indicatorCameraClass[0].setAttribute("position","0.004 -0.240 -0.622");
      indicatorCameraClass[0].setAttribute("rotation","-70.000 0 0");
      indicatorCameraClass[0].setAttribute("opacity","1");
    }
    indicatorCameraClass[0].onclick=(event)=>{
      indicatorCameraClass[0].setAttribute("position","0.004 -0.660-0.622");
      indicatorCameraClass[0].setAttribute("rotation","-90 0 0");
      indicatorCameraClass[0].setAttribute("opacity","0.4");
    }

    indicatorSchroef.setAttribute("class","js--indicatorSchroefOnderkant clickonhover");
    indicatorSchroef.setAttribute("position", "1.140 0.445 -2.014");
    indicatorSchroef.setAttribute("geometry", "primitive: circle; radius: 0.045;");
    indicatorSchroef.setAttribute("color","blue");
    indicatorSchroef.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1300; loop: true;");
    parent[0].appendChild(indicatorSchroef);
    indicatorSchroef.onclick = (event) =>{
      if(itemHolding == "schroef"){
        indicatorSchroef.onclick = null;
        indicatorSchroef.setAttribute("visible","false");
        document.getElementById("js--hold").remove();
        holdingState = "standby";
        let schroefBovenkant = document.createElement("a-entity");
        schroefBovenkant.setAttribute("class", "js--schroefBovenkantRechts");
        schroefBovenkant.setAttribute("gltf-model", "#schroef");
        schroefBovenkant.setAttribute("position", "1.138 0.438 -1.912"); //-2.1 vast
        schroefBovenkant.setAttribute("scale", "0.500 0.550 0.550");
        schroefBovenkant.setAttribute("rotation","90 0 0");
        parent[0].appendChild(schroefBovenkant);
        let schroevendraaierItem = document.createElement("a-entity");
        schroevendraaierItem.setAttribute("class", "js--schroevendraaierIdle2 clickonhover");
        schroevendraaierItem.setAttribute("gltf-model", "#schroevendraaier");
        schroevendraaierItem.setAttribute("position", "1.243 -0.604 -1.575");
        schroevendraaierItem.setAttribute("scale", "0.6 0.6 0.6");
        schroevendraaierItem.setAttribute("rotation","90 -40.000 0");
        parent[0].appendChild(schroevendraaierItem);

        schroevendraaierItem.onclick = (event) =>{
          schroevendraaierItem.onclick = null;
          schroevendraaierItem.setAttribute("model-opacity","0");
          step10_schroevendraaier();
        }
      }
    }
  }
  function step10_schroevendraaier(){
    let schroefBovenkantRechts = document.getElementsByClassName("js--schroefBovenkantRechts");
    let schroevendraaierItemIdle = document.getElementsByClassName("js--schroevendraaierIdle2");
    let schroevendraaierItem = document.createElement("a-entity");
    schroevendraaierItem.setAttribute("class", "js--schroevendraaier clickonhover");
    schroevendraaierItem.setAttribute("gltf-model", "#schroevendraaier");
    schroevendraaierItem.setAttribute("position", "1.141 0.435 -1.475");
    schroevendraaierItem.setAttribute("scale", "0.6 0.6 0.6");
    schroevendraaierItem.setAttribute("rotation","90 0 0");
    parent[0].appendChild(schroevendraaierItem);
    let schroefBovenkantClass = document.getElementsByClassName("js--schroefBovenkant");
    schroevendraaierItem.onclick = (event) =>{
      schroefBovenkantRechts[0].setAttribute("position", "1.138 0.438 -2.1");
      schroevendraaierItem.onclick = null;
      schroevendraaierItem.setAttribute("position", "1.141 0.435 -1.677");
      setTimeout(function(){
        schroevendraaierItem.setAttribute("model-opacity","0");
        let indicatorSchroefClass = document.getElementsByClassName("js--indicatorSchroefOnderkant");
        indicatorSchroefClass[0].setAttribute("visible", "true");
        indicatorSchroefClass[0].setAttribute("position", "1.140 -0.492 -2.014");
        indicatorSchroefClass[0].onclick = (event) =>{
          if(itemHolding == "schroef"){
            indicatorSchroefClass[0].onclick = null;
            indicatorSchroefClass[0].setAttribute("visible","false");
            document.getElementById("js--hold").remove();
            holdingState = "standby";
            let schroefOnderkantRechts = document.createElement("a-entity");
            schroevendraaierItemIdle[0].setAttribute("model-opacity","1");
            schroefOnderkantRechts.setAttribute("class", "js--schroefOnderkantRechts");
            schroefOnderkantRechts.setAttribute("gltf-model", "#schroef");
            schroefOnderkantRechts.setAttribute("position", "1.138 -0.494 -1.912");
            schroefOnderkantRechts.setAttribute("scale", "0.500 0.550 0.550");
            schroefOnderkantRechts.setAttribute("rotation","90 0 0");
            parent[0].appendChild(schroefOnderkantRechts);
            schroevendraaierItemIdle[0].onclick =(event)=>{
              schroevendraaierItemIdle[0].onclick = null;
              schroevendraaierItemIdle[0].setAttribute("model-opacity","0");
              schroevendraaierItem.setAttribute("model-opacity","1");
              schroevendraaierItem.setAttribute("position","1.135 -0.495 -1.475");
              schroevendraaierItem.onclick=(event)=>{
                schroevendraaierItem.onclick= null;
                schroefOnderkantRechts.setAttribute("position", "1.138 -0.494 -2.1");
                schroevendraaierItem.setAttribute("position","1.135 -0.495 -1.677");
                setTimeout(function(){
                  schroevendraaierItem.setAttribute("model-opacity","0");
                  televisionInstructions.setAttribute("src","#stap12");
                  step11();
                },2000)
              }
            }
          }
        }
      },2000);
    }
  }

  function step11(){
    read(11);
    parent[0].setAttribute("rotation","90 0 0");
    parent[0].setAttribute("position","0 -2.64 -3.700");
    let trap = document.createElement("a-entity");
    trap.setAttribute("class", "js--trap clickonhover");
    trap.setAttribute("gltf-model", "#trap");
<<<<<<< HEAD
    trap.setAttribute("position", "-2.760 0.045 -1.880");
    trap.setAttribute("scale", "0.510 1 0.940");
    trap.setAttribute("rotation","-50 -90.000 90.000");
    trap.setAttribute("checkpoint","offset: 1 3.3 0");
=======
    trap.setAttribute("position", "0.535 0.489 -2.602");
    trap.setAttribute("scale", "0.480 1.070 0.570");
    trap.setAttribute("rotation","0 180 0");
    trap.setAttribute("checkpoint","offset: -0.5 2.5 0.6");
>>>>>>> Mauriccio
    parent[0].appendChild(trap);
    let indicatorPlankTop = document.createElement("a-plane");
    indicatorPlankTop.setAttribute("class", "js--indicatorStep2");
    indicatorPlankTop.setAttribute("color","blue");
    indicatorPlankTop.setAttribute("position", "-0.013 -0.050 -5.05");
    indicatorPlankTop.setAttribute("width","3.2");
    indicatorPlankTop.setAttribute("height","0.35");
    indicatorPlankTop.setAttribute("scale","0.760 3.430 0.400");
    indicatorPlankTop.setAttribute("rotation","0 -180 0");
    indicatorPlankTop.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1000; loop: true");
    indicatorPlankTop.setAttribute("material","side:double");
    parent[0].appendChild(indicatorPlankTop);
    indicatorPlankTop.onclick=(event)=>{
      if(itemHolding == "dikkeplank"){
        indicatorPlankTop.onclick = null;
        indicatorPlankTop.setAttribute("visible","false");
        let dikkeplank_a2 = document.createElement("a-entity");
        dikkeplank_a2.setAttribute("class", "js--dikkeplank");
        dikkeplank_a2.setAttribute("gltf-model", "#dikke_plank");
        dikkeplank_a2.setAttribute("position", "-0.002 -0.045 -5.107");
        dikkeplank_a2.setAttribute("scale", "0.48 0.3 0.6");
        dikkeplank_a2.setAttribute("rotation","-90 0 0");
        parent[0].appendChild(dikkeplank_a2); //BOVENKANT
        televisionInstructions.setAttribute("src","#stap12");
        televisionInstructions.setAttribute("opacity","1");
        document.getElementById("js--hold").remove();
        holdingState = "standby";
        step12();

      }
    }
  }
  function step12(){
    televisionInstructions.setAttribute("src","#stap13");
    read(12);
    stateStep = "step12_start";
    let indicatorSchroefTopRechts = document.createElement("a-plane");
    indicatorSchroefTopRechts.setAttribute("class","js--indicatorSchroefTop clickonhover");
    indicatorSchroefTopRechts.setAttribute("position", "-1.132 0.416 -5.162");
    indicatorSchroefTopRechts.setAttribute("rotation", "0 180 180");
    indicatorSchroefTopRechts.setAttribute("geometry", "primitive: circle; radius:0.045;");
    indicatorSchroefTopRechts.setAttribute("color","blue");
    indicatorSchroefTopRechts.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1300; loop: true;");
    parent[0].appendChild(indicatorSchroefTopRechts);


    let indicatorSchroefTopLinks = document.createElement("a-plane");
    indicatorSchroefTopLinks.setAttribute("class","js--indicatorSchroefTop clickonhover");
    indicatorSchroefTopLinks.setAttribute("position", "-1.132 -0.521 -5.162");
    indicatorSchroefTopLinks.setAttribute("rotation", "0 180 180");
    indicatorSchroefTopLinks.setAttribute("geometry", "primitive: circle; radius:0.045;");
    indicatorSchroefTopLinks.setAttribute("color","blue");
    indicatorSchroefTopLinks.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1300; loop: true;");
    parent[0].appendChild(indicatorSchroefTopLinks);

    step12_events(indicatorSchroefTopRechts,indicatorSchroefTopLinks,"links_top_rechts");
    step12_events(indicatorSchroefTopLinks,indicatorSchroefTopRechts,"links_top_links");
  }

  function step12_events(item,item2,positie){
    item.onclick = (event) =>{
      if(itemHolding == "schroef"){
        item.onclick = null;
        item.setAttribute("visible","false");
        item2.onclick = null;
        item2.setAttribute("visible","false");

        let schroevendraaierItem = document.createElement("a-entity");
        schroevendraaierItem.setAttribute("class", "js--schroevendraaier clickonhover");
        schroevendraaierItem.setAttribute("gltf-model", "#schroevendraaier");
        schroevendraaierItem.setAttribute("position", "-0.790 -0.074 -5.260");
        schroevendraaierItem.setAttribute("scale", "0.5 0.5 0.5");
        schroevendraaierItem.setAttribute("rotation","0 0 -40");
        schroevendraaierItem.setAttribute("animation","property: model-opacity; from:0; to: 1; dur: 1000;");
        parent[0].appendChild(schroevendraaierItem);

        let schroefTop = document.createElement("a-entity");
        schroefTop.setAttribute("gltf-model", "#schroef");
        schroefTop.setAttribute("scale", "0.500 0.550 0.550");
        schroefTop.setAttribute("rotation","-90 180 180");
        if(positie =="links_top_rechts"){
          schroefTop.setAttribute("class", "js--lschroefTopRechts");
          schroefTop.setAttribute("position", "-1.133 0.413 -5.279");
        }
        if(positie =="links_top_links"){
          schroefTop.setAttribute("class", "js--lschroefTopLinks");
          schroefTop.setAttribute("position", "-1.133 -0.532 -5.279");
        }
        parent[0].appendChild(schroefTop);

        schroevendraaierItem.onclick=(event)=>{
          schroevendraaierItem.onclick = null;
          if(positie =="links_top_rechts"){
            schroevendraaierItem.setAttribute("position", "-1.136 0.414 -5.652");
            schroevendraaierItem.setAttribute("rotation","-90 0 0");
            schroevendraaierItem.onclick=(event)=>{
              schroevendraaierItem.onclick = null;
              schroevendraaierItem.setAttribute("position", "-1.136 0.414 -5.439");
              schroefTop.setAttribute("position","-1.133 0.413 -5.069")
              setTimeout(function(){
                schroevendraaierItem.setAttribute("animation","property: model-opacity; from:1; to: 0; dur: 1000;");
                if(stateStep == "step12_start"){
                  item2.setAttribute("visible","true");
                  if(schroefTop.getAttribute("class") == "js--lschroefTopRechts"){
                    step12_events(item2,item,"links_top_links");
                  }
                  if(schroefTop.getAttribute("class") == "js--lschroefTopLinks"){
                    step12_events(item2,item,"links_top_rechts");
                  }
                  stateStep = "step12_finish";
                  return;
                }
                if(stateStep == "step12_finish"){
                  schroevendraaierItem.setAttribute("class","js--schroevendraaier");
                  let indicatorsTop = document.getElementsByClassName("js--indicatorSchroefTop");
                  for(let i = 0; i<indicatorsTop.length;i++){
                    indicatorsTop[i].setAttribute("class","js--indicatorSchroefTop");
                  }
                  step12_final()
                  return;
                }
              },1500);
            }
          }

          if(positie =="links_top_links"){
            schroevendraaierItem.setAttribute("position", "-1.136 -0.535 -5.652");
            schroevendraaierItem.setAttribute("rotation","-90 0 0");
            schroevendraaierItem.onclick=(event)=>{
              schroevendraaierItem.onclick = null;
              schroevendraaierItem.setAttribute("position", "-1.136 -0.535 -5.439");
              schroefTop.setAttribute("position","-1.133 -0.532 -5.069");
              setTimeout(function(){
                schroevendraaierItem.setAttribute("animation","property: model-opacity; from:1; to: 0; dur: 1000;");

                if(stateStep == "step12_start"){
                  item2.setAttribute("visible","true");
                  if(schroefTop.getAttribute("class") == "js--lschroefTopRechts"){
                    item2.onclick = (event)=>{step12_events(item2,item,"links_top_links");}
                  }
                  if(schroefTop.getAttribute("class") == "js--lschroefTopLinks"){
                    item2.onclick = (event)=>{step12_events(item2,item,"links_top_rechts");}
                  }
                  stateStep = "step12_finish";
                  return;
                }
                if(stateStep == "step12_finish"){
                  schroevendraaierItem.setAttribute("class","js--schroevendraaier");
                  let indicatorsTop = document.getElementsByClassName("js--indicatorSchroefTop");
                  for(let i = 0; i<indicatorsTop.length;i++){
                    indicatorsTop[i].setAttribute("class","js--indicatorSchroefTop");
                  }
                  step12_final()
                  return;
                }
              },1500);
            }
          }
        }
      }
    }
  }

  function step12_final(){
    stateStep = "step12_start";
<<<<<<< HEAD


    let trapClass = document.getElementsByClassName("js--trap");
    trapClass[0].setAttribute("position", "2.162 0.7 -1.874");
    trapClass[0].setAttribute("scale", "0.510 1 0.940");
    trapClass[0].setAttribute("rotation","-50 90.000 -90.000");
    trapClass[0].setAttribute("checkpoint","offset: -0.3 3.3 -0.75");

=======
>>>>>>> Mauriccio
    let indicatorSchroefTopRechts = document.createElement("a-plane");
    indicatorSchroefTopRechts.setAttribute("class","js--indicatorSchroefTop2 clickonhover");
    indicatorSchroefTopRechts.setAttribute("position", "1.131 0.404 -5.159");
    indicatorSchroefTopRechts.setAttribute("rotation", "0 180 180");
    indicatorSchroefTopRechts.setAttribute("scale", "1 1.250 1");
    indicatorSchroefTopRechts.setAttribute("geometry", "primitive: circle; radius:0.045;");
    indicatorSchroefTopRechts.setAttribute("color","blue");
    indicatorSchroefTopRechts.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1300; loop: true;");
    parent[0].appendChild(indicatorSchroefTopRechts);


    let indicatorSchroefTopLinks = document.createElement("a-plane");
    indicatorSchroefTopLinks.setAttribute("class","js--indicatorSchroefTop2 clickonhover");
    indicatorSchroefTopLinks.setAttribute("position", "1.133 -0.525 -5.161");
    indicatorSchroefTopLinks.setAttribute("rotation", "0 180 180");
    indicatorSchroefTopLinks.setAttribute("scale", "1 1.250 1");
    indicatorSchroefTopLinks.setAttribute("geometry", "primitive: circle; radius:0.045;");
    indicatorSchroefTopLinks.setAttribute("color","blue");
    indicatorSchroefTopLinks.setAttribute("animation","property: opacity; from:0; to: 1; dur: 1300; loop: true;");
    parent[0].appendChild(indicatorSchroefTopLinks);

    step12_events_final(indicatorSchroefTopRechts,indicatorSchroefTopLinks,"rechts_top_rechts");
    step12_events_final(indicatorSchroefTopLinks,indicatorSchroefTopRechts,"rechts_top_links");
  }

  function step12_events_final(item,item2,positie){
    item.onclick = (event) =>{
      if(itemHolding == "schroef"){
        item.onclick = null;
        item.setAttribute("visible","false");
        item2.onclick = null;
        item2.setAttribute("visible","false");

        let schroevendraaierItem = document.createElement("a-entity");
        schroevendraaierItem.setAttribute("class", "js--schroevendraaier clickonhover");
        schroevendraaierItem.setAttribute("gltf-model", "#schroevendraaier");
        schroevendraaierItem.setAttribute("position", "0.780 -0.074 -5.260");
        schroevendraaierItem.setAttribute("scale", "0.5 0.5 0.5");
        schroevendraaierItem.setAttribute("rotation","0 0 40");
        schroevendraaierItem.setAttribute("animation","property: model-opacity; from:0; to: 1; dur: 1000;");
        parent[0].appendChild(schroevendraaierItem);

        let schroefTop = document.createElement("a-entity");
        schroefTop.setAttribute("gltf-model", "#schroef");
        schroefTop.setAttribute("scale", "0.500 0.550 0.550");
        schroefTop.setAttribute("rotation","-90 180 180");
        if(positie =="rechts_top_rechts"){
          schroefTop.setAttribute("class", "js--rschroefTopRechts");
          schroefTop.setAttribute("position", "1.137 0.405 -5.279");
        }
        if(positie =="rechts_top_links"){
          schroefTop.setAttribute("class", "js--rschroefTopLinks");
          schroefTop.setAttribute("position", "1.137 -0.523 -5.279");
        }
        parent[0].appendChild(schroefTop);

        schroevendraaierItem.onclick=(event)=>{
          schroevendraaierItem.onclick = null;
          if(positie =="rechts_top_rechts"){
            schroevendraaierItem.setAttribute("position", "1.135 0.406 -5.640");
            schroevendraaierItem.setAttribute("rotation","-90 0 0");
            schroevendraaierItem.onclick=(event)=>{
              schroevendraaierItem.onclick = null;
              schroevendraaierItem.setAttribute("position", "1.135 0.406 -5.440");
              schroefTop.setAttribute("position","1.137 0.405 -5.063")
              setTimeout(function(){
                schroevendraaierItem.setAttribute("animation","property: model-opacity; from:1; to: 0; dur: 1000;");
                if(stateStep == "step12_start"){
                  item2.setAttribute("visible","true");
                  if(schroefTop.getAttribute("class") == "js--rschroefTopRechts"){
                    step12_events_final(item2,item,"rechts_top_links");
                  }
                  if(schroefTop.getAttribute("class") == "js--rschroefTopLinks"){
                    step12_events_final(item2,item,"rechts_top_rechts");
                  }
                  stateStep = "step12_finish";
                  return;
                }
                if(stateStep == "step12_finish"){
                  console.log("done");
                  read(13);
                  schroevendraaierItem.setAttribute("class","js--schroevendraaier");
                  let indicatorsTop = document.getElementsByClassName("js--indicatorSchroefTop");
                  for(let i = 0; i<indicatorsTop.length;i++){
                    indicatorsTop[i].setAttribute("class","js--indicatorSchroefTop");
                  }
                  return;
                }
              },1500);
            }
          }

          if(positie =="rechts_top_links"){
            schroevendraaierItem.setAttribute("position", "1.135 -0.517 -5.652");
            schroevendraaierItem.setAttribute("rotation","-90 0 0");
            schroevendraaierItem.onclick=(event)=>{
              schroevendraaierItem.onclick = null;
              schroevendraaierItem.setAttribute("position", "1.135 -0.517 -5.440");
              schroefTop.setAttribute("position","1.137 -0.523 -5.063");
              setTimeout(function(){
                schroevendraaierItem.setAttribute("animation","property: model-opacity; from:1; to: 0; dur: 1000;");

                if(stateStep == "step12_start"){
                  item2.setAttribute("visible","true");
                  if(schroefTop.getAttribute("class") == "js--lschroefTopRechts"){
                    item2.onclick = (event)=>{step12_events(item2,item,"links_top_links");}
                  }
                  if(schroefTop.getAttribute("class") == "js--lschroefTopLinks"){
                    item2.onclick = (event)=>{step12_events(item2,item,"links_top_rechts");}
                  }
                  stateStep = "step12_finish";
                  return;
                }
                if(stateStep == "step12_finish"){
                  let trap = document.getElementsByClassName("js--trap");
                  trap[0].setAttribute("opacity","0");
                  trap[0].removeAttribute("checkpoint");
                  console.log("done");
                  schroevendraaierItem.setAttribute("class","js--schroevendraaier");
                  let indicatorsTop = document.getElementsByClassName("js--indicatorSchroefTop");
                  for(let i = 0; i<indicatorsTop.length;i++){
                    indicatorsTop[i].setAttribute("class","js--indicatorSchroefTop");
                  }
                  read(13);
                  return;
                }
              },1500);
            }
          }
        }
      }
    }
  }

  function simulate(){

    cursor.setAttribute("position","0 0 0");
    let dikkeplank_a1 = document.createElement("a-entity");
    dikkeplank_a1.setAttribute("class", "js--pickup");
    dikkeplank_a1.setAttribute("gltf-model", "#dikke_plank");
    dikkeplank_a1.setAttribute("position", "0 -0.045 -2.1");
    dikkeplank_a1.setAttribute("scale", "0.48 0.45 0.6");
    dikkeplank_a1.setAttribute("rotation","90 0 0");
    parent[0].appendChild(dikkeplank_a1); //ONDERKANT

    let dikkeplank_a2 = document.createElement("a-entity");
    dikkeplank_a2.setAttribute("class", "js--dikkeplank");
    dikkeplank_a2.setAttribute("gltf-model", "#dikke_plank");
    dikkeplank_a2.setAttribute("position", "-0.002 -0.045 -5.107");
    dikkeplank_a2.setAttribute("scale", "0.48 0.3 0.6");
    dikkeplank_a2.setAttribute("rotation","-90 0 0");
    //parent[0].appendChild(dikkeplank_a2); //BOVENKANT

    let buitenplank_b1 = document.createElement("a-entity");
    buitenplank_b1.setAttribute("class", "js--pickup");
    buitenplank_b1.setAttribute("gltf-model", "#buiten_plank");
    buitenplank_b1.setAttribute("position", "-1.130 -0.045 -3.618");
    buitenplank_b1.setAttribute("scale", "0.6 0.35 0.6");
    buitenplank_b1.setAttribute("rotation","90 0 180");
    parent[0].appendChild(buitenplank_b1); //RECHTERPLANK

    let buitenplank_b2 = document.createElement("a-entity");
    buitenplank_b2.setAttribute("class", "js--pickup");
    buitenplank_b2.setAttribute("gltf-model", "#buiten_plank");
    buitenplank_b2.setAttribute("position", "1.122 -0.045 -3.618");
    buitenplank_b2.setAttribute("scale", "0.6 0.35 0.6");
    buitenplank_b2.setAttribute("rotation","90 0 0");
    //parent[0].appendChild(buitenplank_b2); //LINKERPLANK


    let middenplank_f1 = document.createElement("a-entity");
    middenplank_f1.setAttribute("class", "js--pickup");
    middenplank_f1.setAttribute("gltf-model", "#midden_plank");
    middenplank_f1.setAttribute("position", "0 -0.045 -3.618");
    middenplank_f1.setAttribute("scale", "0.6 0.35 0.6");
    middenplank_f1.setAttribute("rotation","90 0 0");
    //parent[0].appendChild(middenplank_f1);

    let kleine_plank_c = document.createElement("a-entity");
    kleine_plank_c.setAttribute("class", "js--pickup");
    kleine_plank_c.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_c.setAttribute("position", "-0.55 -0.045 -4.35");
    kleine_plank_c.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_c.setAttribute("rotation","90 90 90");
    //parent[0].appendChild(kleine_plank_c);

    let kleine_plank_d = document.createElement("a-entity");
    kleine_plank_d.setAttribute("class", "js--kleine_plank_midden");
    kleine_plank_d.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_d.setAttribute("position", "0.55 -0.045 -4.35");
    kleine_plank_d.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_d.setAttribute("rotation","90 90 90");
    //parent[0].appendChild(kleine_plank_d);

    let kleine_plank_e = document.createElement("a-entity");
    kleine_plank_e.setAttribute("class", "js--kleine_plank_midden");
    kleine_plank_e.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_e.setAttribute("position", "0.55 -0.045 -3.62");
    kleine_plank_e.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_e.setAttribute("rotation","90 90 90");
    //parent[0].appendChild(kleine_plank_e);

    let kleine_plank_f = document.createElement("a-entity");
    kleine_plank_f.setAttribute("class", "js--pickup");
    kleine_plank_f.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_f.setAttribute("position", "-0.55 -0.045 -3.62");
    kleine_plank_f.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_f.setAttribute("rotation","90 90 90");
    //parent[0].appendChild(kleine_plank_f);

    let kleine_plank_g = document.createElement("a-entity");
    kleine_plank_g.setAttribute("class", "js--kleine_plank_midden");
    kleine_plank_g.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_g.setAttribute("position", "0.55 -0.045 -2.92");
    kleine_plank_g.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_g.setAttribute("rotation","90 90 90");
    //parent[0].appendChild(kleine_plank_g);

    let kleine_plank_h = document.createElement("a-entity");
    kleine_plank_h.setAttribute("class", "js--pickup");
    kleine_plank_h.setAttribute("gltf-model", "#kleine_plank");
    kleine_plank_h.setAttribute("position", "-0.55 -0.045 -2.92");
    kleine_plank_h.setAttribute("scale", "0.47 0.6  0.6");
    kleine_plank_h.setAttribute("rotation","90 90 90");
    //parent[0].appendChild(kleine_plank_h);
    //parent[0].appendChild(dikkeplank_a1);



    let deuvel = document.createElement("a-entity");
    deuvel.setAttribute("class", "js--deuvel_kleineplank");
    deuvel.setAttribute("gltf-model", "#deuvel");
    deuvel.setAttribute("position", "0 0.33 -4.359");
    deuvel.setAttribute("scale", "0.9 0.830 0.650");
    //parent[0].appendChild(deuvel);
    cursorid.setAttribute("position","0 0 0");



    //parent[0].setAttribute("rotation","90 -90 0");
    //parent[0].setAttribute("position","-0.120 -2.64 -3.700");

    step3();
    viewBoxitems("on");
  }
  //simulate();
}
