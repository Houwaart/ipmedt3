// <script src="https://code.responsivevoice.org/responsivevoice.js?key=1WgBn1Cm"></script>
// zet dit in de html, als ik het doe komen er merge conflicts

window.onload = () => {

  const language = "Dutch Male";

  const steps = ["Stap 1, de bodem. Pak de bodem plank (onderdeel A1) uit het menu. Zet het onderdeel op de juiste plek op de mat.", "Stap 2: de linker plank. Pak de zijkant plank (onderdeel B1) uit het menu. Zet het onderdeel op de juiste plek op de bodem plank (A1).", "Stap 3: vastschroeven. pak een schroef (onderdeel s) uit het menu. Verplaats de schroef naar een blauwe cirkel. Schroef deze vast met de schroevendraaier. Herhaal dit nog één keer", "Stap 4: de deuvels. Pak een deuvel (onderdeel D) uit het menu. verplaats de deuvel naar een blauwe cirkel. Herhaal dit tot er geen blauwe cirkels meer zijn.", "Stap 5: plankjes. Pak een klein plankje (onderdeel C) uit het menu. Zet het plankje vast op twee deuvels. Herhaal dit nog 2 keer.", "Stap 6: meer deuvels. Pak een deuvel (onderdeel D) uit het menu. verplaats de deuvel naar een blauwe cirkel. Herhaal dit tot er geen blauwe cirkels meer zijn.", "Stap 7: de midden plank. Pak de midden plank (onderdeel F1) uit het menu. Zet de plank in het midden en maak deze vast aan de deuvels.", "Stap 8: plankjes. Pak een klein plankje (onderdeel C) uit het menu. Zet het plankje vast op twee deuvels. Herhaal dit nog 2 keer.", "Stap 9: nog meer deuvels. Pak een deuvel (onderdeel D) uit het menu. verplaats de deuvel naar een blauwe cirkel. Herhaal dit tot er geen blauwe cirkels meer zijn." "Stap 10: de rechter plank. Pak de zijkant plank (onderdeel B1) uit het menu. Zet het onderdeel op de juiste plek op de bodem plank (A1).", "Stap 11: vastschroeven. pak een schroef (onderdeel s) uit het menu. Verplaats de schroef naar een blauwe cirkel. Schroef deze vast met de schroevendraaier. Herhaal dit nog één keer.", "Stap 12: de bovenkant. Pak de bodem plank (onderdeel A1) uit het menu. Zet het onderdeel op de kast.", "Stap 13: vastschroeven bovenkant. pak een schroef (onderdeel s) uit het menu. Verplaats de schroef naar een blauwe cirkel. Schroef deze vast met de schroevendraaier. Herhaal dit nog drie keer."];

  readText = (text) => {
    responsiveVoice.speak(text, language, {rate: 0.9});
  }

  const read = (stepNumber) => {
    readText(steps[stepNumber]);
  }

  read(0);
  read(1);
  read(2);
  read(3);
  read(4);
  read(5);
  read(6);
  read(7);
  read(8);
  read(9);
  read(10);
  read(11);
  read(12);

} // window onload
